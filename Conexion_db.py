import psycopg2
class Conexion_db:
    def __init__(self,m_host,m_database,m_user,m_pass):
        try:
            self.conn = psycopg2.connect(
                host = m_host,
                database=m_database,
                user=m_user,
                password=m_pass)
            self.cur = self.conn.cursor()
        except:
            print("Error en la conexión")

    def consultar_db(self,query):
        try:
            self.cur.execute(query)
            response = self.cur.fetchall()
            return response
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

    def escribir_db(self,query):
        try:
            self.cur.execute(query)
            self.conn.commit()
            self.cur.close() 
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)

    def cerrar_db(self):
        self.cur.close()
        self.conn.close()
