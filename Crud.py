
from Conexion_db import Conexion_db
class Crud:
    def __init__(self,m_host,m_database,m_user,m_pass):
        self.conn = Conexion_db(m_host,m_database,m_user,m_pass)

    def insertar_cliente(self,apellido,nombre,documento,direccion):
        query = "INSERT INTO \"clientes\"" +\
            "(apellido, nombre, documento, direccion)"+\
	        "VALUES ('"+apellido+"','"+nombre+"','"+documento+"','"+direccion+"')"   
        self.conn.escribir_db(query)

    def consultar_clientes(self,id):
        query= "SELECT * from \"clientes\" WHERE id="+id
        cliente = self.conn.consultar_db(query)
        return cliente

    def consultar_cliente(self):
        query= "SELECT * from \"clientes\" ORDER BY id ASC"
        cliente = self.conn.consultar_db(query)
        return cliente

    def elminar_clientes(self, id):
        query= "DELETE FROM \"clientes\" WHERE id="+id
        self.conn.escribir_db(query) 

    def update_cliente(self,id,apellido,nombre,documento,direccion):
        query= "UPDATE \"clientes\"" +\
            " SET apellido='"+apellido+"', nombre='"+nombre+"' ,documento='"+documento+"' ,direccion='"+direccion+\
            "' WHERE id="+id+";"
        self.conn.escribir_db(query)
        print(query)

    def consultar_producto(self):
        query= "SELECT * from \"producto_terminado\" ORDER BY id ASC"
        producto = self.conn.consultar_db(query)
        return producto

    def close(self):
        self.conn.cerrar_db()
