from flask2 import database
from os import stat

class Cliente(database.Model):
    __tablename__= 'clientes'

    id = database.Column(database.Integer,primary_key=True)
    apellido = database.Column(database.Text, nullable=False)
    nombre = database.Column(database.String, nullable=False)
    documento = database.Column(database.String, nullable=False)
    direccion = database.Column(database.String, nullable=False)

    # Consultar todos los registros de la tabla Clientes
    @staticmethod
    def get_all():
        return Cliente.query.order_by(Cliente.id.asc()).all()

    @staticmethod
    def delete(id_to_delete):
        Cliente.query.filter_by(id=id_to_delete).delete()
        database.session.commit()


