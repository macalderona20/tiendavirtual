from flask import Flask,request,make_response,redirect,render_template
from flask_sqlalchemy import SQLAlchemy
from flask.wrappers import Request, Response
from Crud import Crud
import json
import os

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI']= 'postgresql://jxlbixwqxxvxcm:b5f590a2aa3e0f7e988a1628631ddb6ebb5eaf0d50126cf61a4db6f159bb00b3@ec2-34-233-187-36.compute-1.amazonaws.com:5432/d83hi2su707hqg'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']= False
app.config['Secret_key']= os.urandom(32)
database = SQLAlchemy(app)

from cliente import *

# pagina principal
@app.route('/')
def index():
    return render_template("index.html")

# ver todos los clientes
@app.route('/clientes')
def get_clientes():
    return render_template("mostrar_clientes.html", cliente=Cliente.get_all())

@app.route('/form_agregar_clientes')
def form_agregar_clientes():
    return render_template('form_agregar_clientes.html')

@app.route('/login', methods=["GET","POST"])
def login():
    if(request.method== 'GET'):
        return render_template('login.html')
    if(request.method=='POST'):
        nombreUsuario = request.form.get("username")
        passwd = request.form.get("passwd")
        print("nombre de usuario= "+nombreUsuario+" passwd="+passwd)
    return render_template("login.html")

# ver cliente por posicion especifica "clientes[colocar #]"
@app.route('/cliente')
def get_cliente():
    cr = Crud("ec2-34-233-187-36.compute-1.amazonaws.com","d83hi2su707hqg","jxlbixwqxxvxcm","b5f590a2aa3e0f7e988a1628631ddb6ebb5eaf0d50126cf61a4db6f159bb00b3")
    clientes = cr.consultar_cliente()
    primer_cliente = clientes[2]
    print(primer_cliente)
    respuesta = json.dumps({"id: ": primer_cliente[0],"Apellido: ": primer_cliente[1],"Nombre: ": primer_cliente[2],"Documento: ": primer_cliente[3],"Direccion: ": primer_cliente[4]})
    cr.close()
    return respuesta

# ver producto por posicion especifica "producto[colocar #]"
@app.route('/producto')
def get_producto():
    cr = Crud("ec2-34-233-187-36.compute-1.amazonaws.com","d83hi2su707hqg","jxlbixwqxxvxcm","b5f590a2aa3e0f7e988a1628631ddb6ebb5eaf0d50126cf61a4db6f159bb00b3")
    productos = cr.consultar_producto()
    primer_producto = productos[0]
    print (primer_producto)
    respuesta = json.dumps({"id: ": primer_producto[0],"producto: ": primer_producto[1],"cantidad. ": primer_producto[2]})
    cr.close()
    return respuesta

@app.route('/mostrar_clientes')
def mostrar_clientes():
    return render_template('mostrar_clientes.html')

@app.route('/form_editar_cliente', methods=["GET","POST"])
def form_editar_cliente():
    if(request.method== 'POST'):
        id_cliente=request.form.get("id_cliente")
        cr = Crud("ec2-34-233-187-36.compute-1.amazonaws.com","d83hi2su707hqg","jxlbixwqxxvxcm","b5f590a2aa3e0f7e988a1628631ddb6ebb5eaf0d50126cf61a4db6f159bb00b3")
        cliente = cr.consultar_clientes(id_cliente)
        cr.close
        cliente=cliente[0]
        print(cliente)
        diccionario_clientes={"id":cliente[0],"apellido":cliente[1],"nombre":cliente[2],"documento":cliente[3],"direccion":cliente[4]}
        return render_template('editar_cliente.html', cliente=diccionario_clientes) 


@app.route('/editar_cliente', methods=["GET","POST"])
def editar_cliente():
    if(request.method== 'POST'):
        id=request.form.get("id")
        apellido=request.form.get("apellido")
        nombre=request.form.get("nombre")
        documento=request.form.get("documento")
        direccion=request.form.get("direccion")
        print("id= ",id,"apellido= ",apellido,"nombre= ",nombre,"documento= ",documento,"direccion= ",direccion)
        cr = Crud("ec2-34-233-187-36.compute-1.amazonaws.com","d83hi2su707hqg","jxlbixwqxxvxcm","b5f590a2aa3e0f7e988a1628631ddb6ebb5eaf0d50126cf61a4db6f159bb00b3")
        cr.update_cliente(id,apellido,nombre,documento,direccion)
        cr.close()
        response=make_response(redirect('/clientes'))
        return response

@app.route('/eliminar_cliente', methods=["GET","POST"])
def eliminar_cliente():
        id=request.form.get("id_cliente")        
        Cliente.delete(id)
        response=make_response(redirect('/clientes'))
        return response

@app.route('/agregar_clientes', methods=["GET","POST"])
def agregar_cliente():
    if(request.method== 'POST'):
        apellido=request.form.get("apellido")
        nombre=request.form.get("nombre")
        documento=request.form.get("documento")
        direccion=request.form.get("direccion")
        cr = Crud("ec2-34-233-187-36.compute-1.amazonaws.com","d83hi2su707hqg","jxlbixwqxxvxcm","b5f590a2aa3e0f7e988a1628631ddb6ebb5eaf0d50126cf61a4db6f159bb00b3")
        cr.insertar_cliente(apellido,nombre,documento,direccion)
        cr.close()
        response=make_response(redirect('/clientes'))
        return response

if __name__=="__main__":
    print("Inicializando servidor")
    app.run(debug=True,host='localhost')